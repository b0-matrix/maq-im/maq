use maq_interop::auth::AuthStatus;

use std::convert::TryFrom;
use std::path::{Path, PathBuf};
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use tokio::sync::Mutex;

#[derive(Debug)]
pub enum ClientError {
    Filesystem(String),
    Unknown(Box<dyn std::error::Error>),
}

#[derive(Debug, Clone)]
pub enum SessionTypes {
    Part(String),
    Restored(matrix_sdk::Session),
    Full,
    None,
}

#[derive(Default)]
struct ClientConfig {
    pub db_dir: PathBuf,
    pub file_dir: PathBuf,
}

pub struct Client {
    client: matrix_sdk::Client,

    session: Arc<Mutex<SessionTypes>>,
    auth_status: Arc<Mutex<AuthStatus>>,

    config: Arc<ClientConfig>,

    is_syncing: Arc<AtomicBool>,
}

impl Client {
    const STORE_PREFIX: &'static str = "store";
    const SESSION_PREFIX: &'static str = "session.json";

    pub fn new<U: Into<url::Url>, P: AsRef<Path>>(
        homeserver_url: U,
        db_dir: P,
        file_dir: P,
    ) -> Result<Self, ClientError> {
        let db_dir = db_dir.as_ref().to_path_buf();
        let file_dir = file_dir.as_ref().to_path_buf();

        let store = matrix_sdk::JsonStore::open(db_dir.join(Self::STORE_PREFIX))
            .map_err(|e| ClientError::Unknown(Box::new(e)))?;

        let client_config = matrix_sdk::ClientConfig::new()
            .user_agent("Maq/0 (Unknown) Reqwest/0 (Rust)")
            .unwrap()
            .state_store(Box::new(store));

        let client = matrix_sdk::Client::new_with_config(homeserver_url, client_config)
            .map_err(|e| ClientError::Unknown(Box::new(e)))?;

        let mut session = SessionTypes::None;
        if let Ok(session_file) = std::fs::File::open(db_dir.join(Self::SESSION_PREFIX)) {
            if let Ok(s) = serde_json::from_reader::<_, crate::auth::Session>(session_file)
                .map(matrix_sdk::Session::from)
            {
                session = SessionTypes::Restored(s);
            }
        }

        let mut client_config = ClientConfig::default();
        client_config.file_dir = file_dir;
        client_config.db_dir = db_dir;

        Ok(Client {
            client,

            session: Arc::new(Mutex::new(session)),
            auth_status: Arc::new(Mutex::new(AuthStatus::Unknown)),

            config: Arc::new(client_config),

            is_syncing: Arc::new(AtomicBool::new(false)),
        })
    }

    pub fn db_dir(&self) -> PathBuf {
        self.config.db_dir.clone()
    }

    pub fn file_dir(&self) -> PathBuf {
        self.config.file_dir.clone()
    }
}

impl Client {
    pub async fn auth_status(&self) -> AuthStatus {
        *self.auth_status.lock().await
    }

    pub async fn authorize(&self) -> Result<bool, ClientError> {
        let mut status_lock = self.auth_status.lock().await;

        // Make sure homeserver is compatible for auth
        if *status_lock == AuthStatus::Unknown {
            if let Ok(_) = self
                .client
                .send(matrix_sdk::api::unversioned::get_supported_versions::Request::new())
                .await
            {
                println!("Homeserver online and compatible");

                *status_lock = AuthStatus::WaitUsername;
            }
        }

        let mut session_lock = self.session.lock().await;
        if let SessionTypes::Restored(session) = session_lock.clone() {
            let user_id = session.user_id.clone();
            let result = self.client.restore_login(session).await;

            if result.is_ok() {
                // TODO: Actually verify account details
                match self
                    .client
                    .send(matrix_sdk::api::r0::account::whoami::Request::new())
                    .await
                {
                    Ok(rep) => {
                        if rep.user_id == user_id {
                            *session_lock = SessionTypes::Full;
                            *status_lock = AuthStatus::Ready;

                            return Ok(true);
                        }
                    }
                    Err(e) => {
                        if let matrix_sdk::Error::RumaResponse(resp) = e {
                            if let matrix_sdk::FromHttpResponseError::Http(server_error) = resp {
                                if let matrix_sdk::ServerError::Known(client_error) = server_error {
                                    if let matrix_sdk::api::error::ErrorKind::UnknownToken {
                                        soft_logout,
                                    } = client_error.kind
                                    {
                                        // Session expired
                                        println!("Session expired, logging out");

                                        *status_lock = AuthStatus::WaitPassword;
                                        *session_lock = SessionTypes::Part(user_id.to_string());

                                        return Ok(false);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        Ok(false)
    }

    fn generate_device_name(&self) -> &str {
        "RANDOM_DEVICE_NAME"
    }

    pub async fn send_user_identifier<S: Into<String>>(
        &self,
        user_identifier: S,
    ) -> Result<(), ClientError> {
        let mut status_lock = self.auth_status.lock().await;
        if *status_lock != AuthStatus::WaitUsername && *status_lock != AuthStatus::WaitPassword {
            println!("Cannot modify or initialize user id during this auth status");

            return Ok(());
        }

        let mut username: String = user_identifier.into();
        if let Ok(user_id) = matrix_sdk::identifiers::UserId::try_from(username.clone()) {
            username = user_id.localpart().to_string();
        }

        use matrix_sdk::api::r0::account::get_username_availability;
        let request = get_username_availability::Request::new(&username);
        if let Ok(response) = self.client.send(request).await {
            if response.available {
                // Huh?
                println!("username {} not good, exiting", username);

                return Ok(());
            }
        }

        *status_lock = AuthStatus::WaitPassword;

        let mut session_lock = self.session.lock().await;
        *session_lock = SessionTypes::Part(username);

        Ok(())
    }

    pub async fn send_password<S: Into<String>>(&self, password: S) -> Result<(), ClientError> {
        let mut status_lock = self.auth_status.lock().await;
        if *status_lock != AuthStatus::WaitPassword {
            println!("Cannot modify or initialize password during this auth status");

            return Ok(());
        }

        let username_opt: Option<String> =
            if let SessionTypes::Part(username) = &*self.session.lock().await {
                Some(username.clone())
            } else {
                None
            };

        if let Some(username) = username_opt {
            let response = self
                .client
                .login(
                    username.as_str(),
                    password.into().as_str(),
                    None,
                    Some(self.generate_device_name()),
                )
                .await
                .map_err(|e| ClientError::Unknown(Box::new(e)))?;

            *self.session.lock().await = SessionTypes::Full;

            let session = matrix_sdk::Session {
                access_token: response.access_token,
                user_id: response.user_id,
                device_id: response.device_id,
            };
            let session_file = std::fs::OpenOptions::new()
                .write(true)
                .create(true)
                .open(self.db_dir().join(Self::SESSION_PREFIX))
                .map_err(|e| ClientError::Unknown(Box::new(e)))?;

            serde_json::to_writer(session_file, &crate::auth::Session::from(session))
                .map_err(|e| ClientError::Unknown(Box::new(e)))?;

            *status_lock = AuthStatus::Ready;
        }

        Ok(())
    }
}

impl Client {
    pub async fn user_id(&self) -> Option<matrix_sdk::identifiers::UserId> {
        self.client.user_id().await
    }
}

impl Client {
    pub async fn add_event_emitter(&mut self, emitter: Box<dyn matrix_sdk::EventEmitter>) {
        self.client.add_event_emitter(emitter).await
    }

    pub fn sync(&self) {
        use matrix_sdk::api::r0::filter::{
            FilterDefinition, LazyLoadOptions, RoomEventFilter, RoomFilter,
        };
        use matrix_sdk::api::r0::sync::sync_events::Filter;

        if self.is_syncing.load(Ordering::SeqCst) {
            println!("Already syncing, ignoring");

            return;
        }
        self.is_syncing.store(true, Ordering::SeqCst);

        let client = self.client.clone();
        let is_syncing = self.is_syncing.clone();

        tokio::spawn(async move {
            // let allowed_event_types = ["m.room.message".into()];

            let mut room_filter = RoomFilter::default();
            let mut room_event_filter = RoomEventFilter::default();
            // room_event_filter.types = Some(&allowed_event_types);
            room_event_filter.lazy_load_options = LazyLoadOptions::Enabled {
                include_redundant_members: false,
            };
            room_filter.timeline = room_event_filter;

            let mut filter_definition = FilterDefinition::default();
            filter_definition.room = room_filter;
            let filter = Filter::FilterDefinition(filter_definition);

            let sync_settings = matrix_sdk::SyncSettings::default().filter(filter);

            client
                .sync_with_callback(sync_settings, |resp| async {
                    if !is_syncing.load(Ordering::SeqCst) {
                        return matrix_sdk::LoopCtrl::Break;
                    }

                    matrix_sdk::LoopCtrl::Continue
                })
                .await
        });
    }
}
