use crate::client::Client;
use matrix_sdk::{
    events::{
        room::message::{MessageEventContent, TextMessageEventContent},
        AnyMessageEventContent, SyncMessageEvent,
    },
    ClientConfig, EventEmitter, JsonStore, SyncRoom, SyncSettings,
};

struct OneTimeClientCreds {
    homeserver_url: String,

    db_dir: String,
    file_dir: String,

    user_id: String,
    user_password: String,
}

fn one_time_client_creds_from_env() -> OneTimeClientCreds {
    use std::env;

    let homeserver_url = env::var("HOMESERVER_URL").unwrap();
    let db_dir = env::var("DB_DIR").unwrap();
    let file_dir = env::var("FILE_DIR").unwrap();
    let user_id = env::var("USER_ID").unwrap();
    let user_password = env::var("USER_PASSWORD").unwrap();

    OneTimeClientCreds {
        homeserver_url,
        db_dir,
        file_dir,
        user_id,
        user_password,
    }
}

async fn obtain_onetime_client() -> Client {
    let conf = one_time_client_creds_from_env();

    let c = Client::new(
        url::Url::parse(conf.homeserver_url.as_str()).unwrap(),
        conf.db_dir,
        conf.file_dir,
    )
    .unwrap();

    loop {
        c.authorize().await.unwrap();
        let status = c.auth_status().await;

        use maq_interop::auth::AuthStatus;
        match status {
            AuthStatus::WaitUsername => {
                c.send_user_identifier(conf.user_id.clone()).await.unwrap();
            }
            AuthStatus::WaitPassword => {
                c.send_password(conf.user_password.clone()).await.unwrap();
            }
            AuthStatus::Ready => {
                break;
            }
            _ => {}
        }
    }

    c
}

struct TestEmitter;

#[async_trait::async_trait]
impl EventEmitter for TestEmitter {
    async fn on_room_message(&self, room: SyncRoom, event: &SyncMessageEvent<MessageEventContent>) {
        if let SyncRoom::Joined(room) = room {
            let msg_body = if let SyncMessageEvent {
                content: MessageEventContent::Text(TextMessageEventContent { body: msg_body, .. }),
                ..
            } = event
            {
                msg_body.clone()
            } else {
                String::new()
            };

            println!("{}", msg_body);
        }
    }
}

#[tokio::test]
async fn client_new() {
    let mut c = obtain_onetime_client().await;

    println!("user_id: {}", c.user_id().await.unwrap());

    println!("Client ready");

    c.add_event_emitter(Box::new(TestEmitter {})).await;

    c.sync();

    tokio::time::delay_for(tokio::time::Duration::from_secs(20)).await;
}
