#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Req {
    #[prost(string, tag="10000")]
    pub extra: std::string::String,
    #[prost(oneof="req::Entry", tags="1, 2, 3, 4")]
    pub entry: ::std::option::Option<req::Entry>,
}
pub mod req {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Entry {
        #[prost(message, tag="1")]
        AuthorizationStatus(super::authorization_status::Req),
        #[prost(message, tag="2")]
        Authorize(super::authorize::Req),
        #[prost(message, tag="3")]
        SendUserIdentifier(super::send_user_identifier::Req),
        #[prost(message, tag="4")]
        SendPassword(super::send_password::Req),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ReceivedInfo {
    #[prost(oneof="received_info::Entry", tags="1, 2")]
    pub entry: ::std::option::Option<received_info::Entry>,
}
pub mod received_info {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Entry {
        #[prost(message, tag="1")]
        Update(super::super::update::Update),
        #[prost(message, tag="2")]
        Rep(super::Rep),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Error {
    #[prost(string, tag="1")]
    pub kind: std::string::String,
    #[prost(string, tag="2")]
    pub description: std::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Rep {
    #[prost(string, tag="10000")]
    pub extra: std::string::String,
    #[prost(oneof="rep::Entry", tags="1, 2, 3, 4, 9999")]
    pub entry: ::std::option::Option<rep::Entry>,
}
pub mod rep {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Entry {
        #[prost(message, tag="1")]
        AuthorizationStatus(super::authorization_status::Rep),
        #[prost(message, tag="2")]
        Authorize(super::authorize::Rep),
        #[prost(message, tag="3")]
        SendUserIdentifier(super::send_user_identifier::Rep),
        #[prost(message, tag="4")]
        SendPassword(super::send_password::Rep),
        #[prost(message, tag="9999")]
        Error(super::Error),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct AuthorizationStatus {
}
pub mod authorization_status {
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Req {
    }
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Rep {
        #[prost(enumeration="super::super::auth::AuthStatus", tag="1")]
        pub status: i32,
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Authorize {
}
pub mod authorize {
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Req {
    }
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Rep {
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct SendUserIdentifier {
}
pub mod send_user_identifier {
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Req {
        #[prost(string, tag="1")]
        pub user_identifier: std::string::String,
    }
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Rep {
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct SendPassword {
}
pub mod send_password {
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Req {
        #[prost(string, tag="1")]
        pub password: std::string::String,
    }
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Rep {
    }
}
