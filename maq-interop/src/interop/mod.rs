#[path = "im.maq.base.rs"]
pub mod base;

#[path = "im.maq.auth.rs"]
pub mod auth;

#[path = "im.maq.reqrep.rs"]
mod _reqrep;
pub mod reqrep {
    pub use super::_reqrep::{
        authorization_status, authorize, send_password, send_user_identifier,
    };
    pub use super::_reqrep::{ReceivedInfo, Rep, Req};

    pub use super::_reqrep::received_info::Entry as ReceivedInfoEntry;
    pub use super::_reqrep::rep::Entry as RepEntry;
    pub use super::_reqrep::req::Entry as ReqEntry;

    use prost::Message;

    impl ReceivedInfo {
        pub fn from_update(update: super::update::Update) -> Self {
            Self {
                entry: Some(ReceivedInfoEntry::Update(update)),
            }
        }

        pub fn from_rep(rep: Rep) -> Self {
            Self {
                entry: Some(ReceivedInfoEntry::Rep(rep)),
            }
        }

        pub fn to_proto(&self) -> bytes::Bytes {
            let mut buf = bytes::BytesMut::new();
            self.encode(&mut buf).unwrap();
            buf.freeze()
        }
    }

    impl Req {
        pub fn new(entry: ReqEntry, extra: String) -> Self {
            Self {
                entry: Some(entry),
                extra,
            }
        }

        pub fn to_proto(&self) -> bytes::Bytes {
            let mut buf = bytes::BytesMut::new();
            self.encode(&mut buf).unwrap();
            buf.freeze()
        }
    }

    impl Rep {
        pub fn new(entry: RepEntry, extra: String) -> Self {
            Self {
                entry: Some(entry),
                extra,
            }
        }
    }
}

#[path = "im.maq.update.rs"]
pub mod update;
