#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
#[repr(i32)]
pub enum AuthStatus {
    Unknown = 0,
    WaitUsername = 100,
    WaitPassword = 101,
    Ready = 200,
    LoggingOut = 1000,
    Closing = 1100,
    Closed = 1101,
}
