#[derive(Clone, PartialEq, ::prost::Message)]
pub struct MxcUri {
    #[prost(string, tag="1")]
    pub server_name: std::string::String,
    #[prost(string, tag="2")]
    pub file_id: std::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct RoomId {
    #[prost(string, tag="1")]
    pub server_name: std::string::String,
    #[prost(string, tag="2")]
    pub id: std::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct RoomAlias {
    #[prost(string, tag="1")]
    pub server_name: std::string::String,
    #[prost(string, tag="2")]
    pub alias: std::string::String,
}
